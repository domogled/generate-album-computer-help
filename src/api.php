<?php

function get_albums(int $userId): \GuzzleHttp\Psr7\Stream
{
    $url = API_URL . "/albums?userId=$userId";

    return http_get($url);

}

function get_photos(int $albumId): \GuzzleHttp\Psr7\Stream
{
    $url = API_URL . "/photos?albumId=$albumId";

    return http_get($url);

}

function http_get(string $url): \GuzzleHttp\Psr7\Stream
{

    // ------------- very simply solution ---------------------
    // $data = file_get_contents($url);

    // ------------- php-http extension ---------------------

    // direct using php-http extension - not works - see Note.md

    // $extensions = get_loaded_extensions();

    // $ext_funcs = get_extension_funcs('http');

    // var_dump($extensions);
    // var_dump($ext_funcs);

    // $r = new HttpRequest($url, HttpRequest::METH_GET);
    // $r->addQueryData(array('userId' => $userId));

    // try {
    //     $r->send();
    //     if ($r->getResponseCode() == 200) {
    //         echo $r->getResponseBody();
    //     }
    // } catch (HttpException $ex) {
    //     echo $ex;
    // }

    // ------------- via guzzle library ---------------------

    $client = new \GuzzleHttp\Client(['http_errors' => true]);
    $request = $client->request(GET, $url);

    $http_code = $request->getStatusCode();

    // dont need if $client = new \GuzzleHttp\Client(['http_errors' => true]);
    // if ($http_code >= 400) {
    //     throw new \Exception("Http error code $http_code for $url");
    // }

    $content_type = trim(
        explode(
            ';',
            $request->getHeaderLine('content-type')
        )[0]
    ); # 'application/json; charset=utf8'

    if ($content_type != 'application/json') {
        throw new \Exception("Content type $content_type failed. Expect application/json.");
    }

    $data = $request->getBody();

    // foreach ($data as $album) {
    //     yield $album;
    // }
    return $data;
}

function decode_stream(\GuzzleHttp\Psr7\Stream $stream): array
{
    $json = $stream->getContents();

    return json_decode($json, true/*, JSON_THROW_ON_ERROR Available since PHP 7.3.0. */);

}
